package controllers

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func isLoggedIn(c *gin.Context) bool {
	username := getSessionString(c, "username")
	verified := getSessionBool(c, "verified")

	if len(username) > 0 && verified {
		return true
	}
	return false
}

func getSessionString(c *gin.Context, key string) string {
	session := sessions.Default(c)
	if v := session.Get(key); v != nil {
		return v.(string)
	}
	return ""
}

func getSessionInt(c *gin.Context, key string) int {
	session := sessions.Default(c)
	if v := session.Get(key); v != nil {
		return v.(int)
	}
	return 0
}

func getSessionBool(c *gin.Context, key string) bool {
	session := sessions.Default(c)
	if v := session.Get(key); v != nil {
		return v.(bool)
	}
	return false
}
