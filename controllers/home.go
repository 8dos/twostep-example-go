package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// ShowHomePage shows the home page
func ShowHomePage(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", gin.H{
		"title": "Welcome",
	})
}
