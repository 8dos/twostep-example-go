package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// NotFound is called when a route is not found.
func NotFound(c *gin.Context) {
	c.HTML(http.StatusNotFound, "404.html", gin.H{
		"title": "Error",
	})
}
