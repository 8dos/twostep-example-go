package controllers

import (
	"database/sql"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/objectia/example-go/models"
)

// ShowSignup shows the signup page
func ShowSignup(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("username")
	if v != nil {
		username := v.(string)
		if len(username) > 0 {
			// Already logged in
			c.Redirect(http.StatusFound, "/account")
			return
		}
	}

	c.HTML(http.StatusOK, "signup.html", gin.H{
		"title": "Sign up",
	})
}

type signupParams struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// Signup processes signup requests
func Signup(c *gin.Context) {
	var params signupParams
	err := c.ShouldBindJSON(&params)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": "Missing username and/or password",
		})
		return
	}

	_, err = models.FetchUser(params.Username)
	if err != nil && err != sql.ErrNoRows {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": "That user already exists",
		})
		return
	}

	user := &models.User{
		Username: params.Username,
		Password: params.Password, // Not encrypted. THis is just a sample, don't do this in real code!
	}

	err = user.Insert()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"success": false,
			"message": "Could not save user",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"success": true,
	})
}
