package controllers

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/objectia/example-go/models"
	twostep "github.com/objectia/twostep-go"
)

// ShowLogin shows the login page
func ShowLogin(c *gin.Context) {
	if isLoggedIn(c) {
		c.Redirect(http.StatusFound, "/account")
		return
	}

	c.HTML(http.StatusOK, "login.html", gin.H{
		"title": "Login",
	})
}

type loginParams struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// Login processes login requests
func Login(c *gin.Context) {
	var params loginParams
	err := c.ShouldBindJSON(&params)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": "Missing username and/or password",
		})
		return
	}

	user, err := models.FetchUser(params.Username)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": "Wrong username",
		})
		return
	}

	if user.Password != params.Password {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": "Wrong password",
		})
		return
	}

	// Update session
	session := sessions.Default(c)
	session.Set("username", params.Username)
	session.Set("verified", true)
	if len(user.TwofactorMethod) > 0 {
		session.Set("verified", false)
	}
	session.Save()

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"success": true,
	})
}

// ShowLogin2FA shows the login verify page
func ShowLogin2FA(c *gin.Context) {
	c.HTML(http.StatusOK, "login-verify.html", gin.H{
		"title": "Login 2FA",
	})
}

// Login2FA processes  requests
func Login2FA(c *gin.Context) {
	var params verifyParams
	err := c.ShouldBindJSON(&params)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": "Missing code",
		})
		return
	}

	username := getSessionString(c, "username")
	user, _ := models.FetchUser(username)
	twofactor := c.MustGet("twofactor").(*twostep.Client)

	_ = twofactor

	if user.TwofactorMethod == "otp" {
		/*err = twofactor.VerifyCode(user.TwofactorSecret, params.Code)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"status":  http.StatusUnauthorized,
				"success": false,
				"message": "Sorry! Wrong code",
			})
			return
		}*/
	} else {
		//FIXME....
	}

	session := sessions.Default(c)
	session.Set("verified", true)
	session.Save()

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"success": true,
	})
}

// Logout logs the user out
func Logout(c *gin.Context) {

	// Destroy session
	session := sessions.Default(c)
	session.Clear()
	session.Save()

	c.Redirect(http.StatusFound, "/")
}
