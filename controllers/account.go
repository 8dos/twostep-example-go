package controllers

import (
	"fmt"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/objectia/example-go/models"
	twostep "github.com/objectia/twostep-go"
)

type initParams struct {
	Method      string `json:"method" binding:"required"`
	CountryCode int    `json:"country_code" binding:"required"`
	Phone       string `json:"phone" binding:"required"`
	Email       string `json:"email" binding:"required"`
}

type verifyParams struct {
	Code string `json:"code" binding:"required"`
}

// ShowAccount shows the account page
// GET /account
func ShowAccount(c *gin.Context) {
	username := getSessionString(c, "username")
	user, err := models.FetchUser(username)
	if err != nil {
		c.HTML(http.StatusNotFound, "404.html", gin.H{
			"title": "Error",
		})
		return
	}

	c.HTML(http.StatusOK, "account.html", gin.H{
		"title":    "My account",
		"username": user.Username,
		"enabled":  len(user.TwofactorMethod) > 0,
	})
}

// Enable2FA shows the Enable 2FA page
// GET /twofactor/enable
func Enable2FA(c *gin.Context) {
	c.HTML(http.StatusOK, "twofactor-enable.html", gin.H{
		"title": "Enable two-factor authentication",
	})
}

// Disable2FA disabled two-factor auth
// GET /twofactor/disable
func Disable2FA(c *gin.Context) {
	username := getSessionString(c, "username")
	user, _ := models.FetchUser(username)
	user.TwofactorMethod = ""
	user.TwofactorSecret = ""
	user.Update()

	c.Redirect(http.StatusFound, "/account")
}

// ShowEnableOTP shows the Enable OTP page
func ShowEnableOTP(c *gin.Context) {
	/*twofactor := c.MustGet("twofactor").(*twostep.Client)

	// Create secret
	secret, _ := twofactor.CreateSecret("SHA1")

	// Remember secret for later...
	session := sessions.Default(c)
	session.Set("secret", secret)
	session.Save()

	issuer := "MyCompany" //TODO: change to your organization
	user := getSessionString(c, "username")

	imageURL, _ := twofactor.GetQRUrl(secret, issuer, user)

	c.HTML(http.StatusOK, "twofactor-otp.html", gin.H{
		"title": "Enable two-factor authentication",
		"image": imageURL,
	})*/
}

// EnableOTP enables OTP auth
func EnableOTP(c *gin.Context) {
	var params verifyParams
	err := c.ShouldBindJSON(&params)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": "Missing code",
		})
		return
	}

	username := getSessionString(c, "username")
	secret := getSessionString(c, "secret")
	twofactor := c.MustGet("twofactor").(*twostep.Client)

	_ = twofactor

	// Validate code
	/*err = twofactor.VerifyCode(secret, params.Code)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"status":  http.StatusUnauthorized,
			"success": false,
			"message": "Sorry! Wrong code",
		})
		return
	}*/

	// Update user database
	user, _ := models.FetchUser(username)
	user.TwofactorMethod = "otp"
	user.TwofactorSecret = secret
	user.Update()

	// Update session
	session := sessions.Default(c)
	session.Delete("secret")
	session.Save()

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"success": true,
	})
}

// Init2FA -
// POST /twofactor/init
func Init2FA(c *gin.Context) {
	var params initParams
	err := c.ShouldBindJSON(&params)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": "Missing required parameter(s)",
		})
		return
	}

	twofactor := c.MustGet("twofactor").(*twostep.Client)

	// CreateUser
	options := twostep.NewParameters()
	options.Add("email", params.Email)
	options.Add("phone", params.Phone)
	options.Add("country_code", params.CountryCode)
	user, err := twofactor.CreateUser(options)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  http.StatusInternalServerError,
			"success": false,
			"message": "Failed to create user",
		})
		return
	}
	// Remember UserID for later...
	session := sessions.Default(c)
	session.Set("userid", user.ID)

	if params.Method == "SMS" {
		// Request a SMS
		//options := twostep.NewParameters()
		//options.Add("locale", "en")
		_, err = twofactor.RequestSMS(user.ID, nil)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"status":  http.StatusInternalServerError,
				"success": false,
				"message": "Failed to request sms",
			})
			return
		}
	} else if params.Method == "PUSH" {
		options := twostep.NewParameters()
		options.Add("message", "Login from MyApp requested")
		notification, err := twofactor.RequestPushNotification(user.ID, options)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"status":  http.StatusInternalServerError,
				"success": false,
				"message": "Failed to request push notification",
			})
			return
		}
		session.Set("push_uuid", notification.UUID)
	} else {
		// TOTP
	}

	session.Save()

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"success": true,
	})
}

// Verify2FA -
// POST /twofactor/verify
func Verify2FA(c *gin.Context) {
	var params verifyParams
	err := c.ShouldBindJSON(&params)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"success": false,
			"message": "Missing code",
		})
		return
	}

	username := getSessionString(c, "username")
	userID := getSessionInt(c, "userid")
	twofactor := c.MustGet("twofactor").(*twostep.Client)

	// Validate code
	err = twofactor.VerifyCode(userID, params.Code)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"status":  http.StatusUnauthorized,
			"success": false,
			"message": "Sorry! Wrong code",
		})
		return
	}

	// Update session
	session := sessions.Default(c)
	session.Delete("userid")
	session.Save()

	// Update user database
	user, _ := models.FetchUser(username)
	user.TwofactorMethod = "sms"
	user.TwofactorUserID = userID
	user.Update()

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"success": true,
	})
}

// CheckPushStatus -
// GET /twofactor/approval
func CheckPushStatus(c *gin.Context) {
	username := getSessionString(c, "username")
	userID := getSessionInt(c, "userid")
	pushUUID := getSessionString(c, "push_uuid")

	twofactor := c.MustGet("twofactor").(*twostep.Client)

	status, err := twofactor.CheckPushNotification(userID, pushUUID)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"status":  http.StatusUnauthorized,
			"success": false,
			"message": "Failed to check status",
		})
		return
	}

	fmt.Println(status)

	if status.Status == "accepted" {
		// Update user database
		user, _ := models.FetchUser(username)
		user.TwofactorMethod = "push"
		user.TwofactorUserID = userID
		user.Update()
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"success": true,
		"data": gin.H{
			"status": status.Status,
		},
	})
}
