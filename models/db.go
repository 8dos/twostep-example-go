package models

import (
	"log"

	"github.com/samonzeweb/godb"
	"github.com/samonzeweb/godb/adapters/sqlite"
)

var _db *godb.DB

func init() {
	log.Printf("Init database...")
	var err error
	_db, err = godb.Open(sqlite.Adapter, "./demo.db")
	panicIfErr(err)

	createTable := `CREATE TABLE IF NOT EXISTS User (
		username TEXT NOT NULL PRIMARY KEY,
		password TEXT NOT NULL,
		twofactor_method TEXT NULL,
		twofactor_secret TEXT NULL,
		twofactor_user_id INTEGER NULL
	);
	`
	_, err = _db.CurrentDB().Exec(createTable)
	panicIfErr(err)
}

// It's just an example, what did you expect ? (never do that in real code)
func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
