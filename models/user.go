package models

// User model
type User struct {
	Username        string `db:"username,key"`
	Password        string `db:"password"`
	TwofactorMethod string `db:"twofactor_method"`
	TwofactorSecret string `db:"twofactor_secret"`
	TwofactorUserID int    `db:"twofactor_user_id"`
}

// Insert adds a new user record
func (u *User) Insert() error {
	return _db.Insert(u).Do()
}

// Update updates an existing user record
func (u *User) Update() error {
	return _db.Update(u).Do()
}

// FetchUser selects a single user by username
func FetchUser(id string) (*User, error) {
	user := User{}
	err := _db.Select(&user).
		Where("username = ?", id).
		Do()
	if err != nil {
		return nil, err
	}
	return &user, nil
}
