package main

import "github.com/objectia/example-go/router"
import _ "github.com/objectia/example-go/models" // init database

func main() {
	r := router.NewRouter()

	port := "8080"
	r.Run(":" + port)
}

/*
func main() {
	ClientID := os.Getenv("TWOSTEP_CLIENT_ID")
	ClientSecret := os.Getenv("TWOSTEP_CLIENT_SECRET")

	r := gin.Default()

	// Middleware
	store := sessions.NewCookieStore([]byte("secret"))
	r.Use(sessions.Sessions("mysession", store))

	r.LoadHTMLGlob("views/*")

	// Show home page
	r.GET("/", func(c *gin.Context) {
		validated := false
		session := sessions.Default(c)
		v := session.Get("validated")
		if v != nil {
			validated = v.(bool)
		}
		enabled := false
		v = session.Get("enabled")
		if v != nil {
			enabled = v.(bool)
		}

		c.HTML(http.StatusOK, "index.html", gin.H{
			"title":     "Home",
			"validated": validated,
			"enabled":   enabled,
		})
	})

	// Show enable two-factor auth page
	r.GET("/enable", func(c *gin.Context) {
		session := sessions.Default(c)

		config := twostep.Config{
			ClientID:     ClientID,
			ClientSecret: ClientSecret,
		}

		cli, err := twostep.NewClient(config, nil)
		if err != nil {

		}

		secret, _ := cli.Init("SHA1")
		url, _ := cli.QRUrl(secret, "mycompany", "joey@user.com")

		// Store secret in session
		session.Set("secret", secret)
		session.Save()

		c.HTML(http.StatusOK, "enable.html", gin.H{
			"title": "Enable",
			"url":   url,
		})
	})

	// Enable two-factor auth
	r.POST("/enable", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "OK",
		})
	})

	// Show validate page
	r.GET("/validate", func(c *gin.Context) {
		c.HTML(http.StatusOK, "validate.html", gin.H{
			"title": "Login",
		})
	})

	type Input struct {
		Code string `json:"code" form:"code" binding:"required"`
	}

	// Process validation
	r.POST("/validate", func(c *gin.Context) {
		// Get code from post body
		var input Input
		err := c.Bind(&input)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": "Code is a required parameter",
			})
			return
		}

		// Get secret from session
		session := sessions.Default(c)
		v := session.Get("secret")
		if v == nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"message": "Invalid request",
			})
			return
		}
		secret := v.(string)

		cli := twostep.New(apiKey, nil)
		err = cli.Validate(secret, input.Code)
		if err != nil {
			// Code was incorrect
			c.JSON(http.StatusUnauthorized, gin.H{
				"message": "Incorrect code",
			})
			return
		}

		// Success
		c.JSON(http.StatusOK, gin.H{
			"message": "OK",
		})
	})

	r.Run(":8080") // listen and serve on 0.0.0.0:8080
}
*/
