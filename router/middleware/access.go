package middleware

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

// IsLoggedIn checks if user is authenticated.
func IsLoggedIn(c *gin.Context) {
	username := ""
	verified := false

	session := sessions.Default(c)
	if v := session.Get("username"); v != nil {
		username = v.(string)
	}
	if v := session.Get("verified"); v != nil {
		verified = v.(bool)
	}

	if len(username) > 0 {
		if verified {
			c.Next()
		} else {
			c.Redirect(http.StatusTemporaryRedirect, "/login/verify")
		}
	} else {
		c.Redirect(http.StatusTemporaryRedirect, "/login")
	}
}
