package middleware

import (
	"github.com/gin-gonic/gin"
	twostep "github.com/objectia/twostep-go"
)

var conf = twostep.Config{
	ClientID:     "12345",
	ClientSecret: "67890",
	Sandbox:      true,
	//TokenStore:   tokenStore{}, //FIXME
}

// Twofactor injects the twostep client into context
func Twofactor(c *gin.Context) {
	cli, err := twostep.NewClient(conf, nil)
	if err != nil {
		panic(err)
	}

	c.Set("twofactor", cli)
	c.Next()
}
