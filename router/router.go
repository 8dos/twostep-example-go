package router

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/objectia/example-go/controllers"
	"github.com/objectia/example-go/router/middleware"
)

// NewRouter sets up routing with middlewares.
func NewRouter() *gin.Engine {
	r := gin.Default()

	// Middlewares
	store := sessions.NewCookieStore([]byte("secret"))
	r.Use(sessions.Sessions("mysession", store))

	r.Use(middleware.Twofactor)

	// Static files
	r.Static("/assets", "./assets")
	r.StaticFile("/favicon.ico", "./assets/favicon.ico")

	// Views
	r.LoadHTMLGlob("views/*")

	// Routes - public
	r.GET("/", controllers.ShowHomePage)
	r.GET("/signup", controllers.ShowSignup)
	r.POST("/signup", controllers.Signup)
	r.GET("/login", controllers.ShowLogin)
	r.POST("/login", controllers.Login)
	r.GET("/login/verify", controllers.ShowLogin2FA)
	r.POST("/login/verify", controllers.Login2FA)
	r.GET("/logout", controllers.Logout)

	r.Use(middleware.IsLoggedIn)

	// Protected routes
	r.GET("/account", controllers.ShowAccount)
	r.GET("/twofactor/enable", controllers.Enable2FA)
	r.GET("/twofactor/disable", controllers.Disable2FA)

	r.GET("/twofactor/otp", controllers.ShowEnableOTP)
	r.POST("/twofactor/otp", controllers.EnableOTP)
	r.POST("/twofactor/init", controllers.Init2FA)
	r.POST("/twofactor/verify", controllers.Verify2FA)

	r.GET("/twofactor/approval", controllers.CheckPushStatus)

	/*

			// Public
			v1.POST("/users", users.CreateUser)
			v1.GET("/users/:id", users.GetUser)
			v1.POST("/users/:id/sms", users.RequestSMS)
			v1.POST("/users/:id/call", users.RequestCall)
			//v1.POST("/verify", controllers.Verify)
			v1.DELETE("/users/:id", users.DeleteUser)

			v1.POST("/totp/secret", controllers.CreateSecret) //FIXME.....
			v1.GET("/totp/qr", controllers.QRCode)
			v1.GET("/totp/validate", controllers.Validate)

			// Private
			v1.GET("/ping", ping.Ping)
			v1.GET("/geoip/:ipaddress", geoip.Lookup)

			v1.POST("/accounts", accounts.Create)
			v1.GET("/accounts/:id", accounts.Get)
			v1.PUT("/accounts/:id", accounts.Update)
			v1.PUT("/accounts/:id/activate", accounts.Activate)
			v1.PUT("/accounts/:id/cancel", accounts.Cancel)
			v1.PUT("/accounts/:id/resume", accounts.Resume)
			v1.DELETE("/accounts/:id", accounts.Delete)
		}
	*/
	// Route not found handler
	r.NoRoute(controllers.NotFound)

	return r
}
